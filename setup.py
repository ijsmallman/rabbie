from distutils.core import setup

exec(open('rabbie/version.py').read())

setup(
    name='rabbie',
    version=__version__,
    author='Joe Smallman',
    author_email='ijsmallman@gmail.com',
    description='Package to add smart home monitoring to holiday cottage in Scotland',
    packages=[
      'rabbie',
      'rabbie.camera',
      'rabbie.level_sensor',
      'rabbie.database',
      'rabbie.database.models'
    ],
    package_data={'rabbie': ['schemas/config_schema.json']}
)
