from datetime import datetime
from os.path import exists, getmtime
from time import sleep

from rabbie import capture_image, Config


def test_capture():

    t0 = datetime.utcnow().replace(microsecond=0)

    sleep(1)

    image_path = capture_image()

    sleep(1)

    assert exists(image_path)

    dt = getmtime(image_path)

    t1 = datetime.utcfromtimestamp(dt)

    t2 = datetime.utcnow().replace(microsecond=0)

    assert (t1 - t0).total_seconds() > 0

    assert (t2 - t1).total_seconds() > 0
