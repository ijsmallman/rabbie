import logging
from os.path import join, dirname

import pytest

from rabbie import Config, ScheduledTask, ConfigError
from rabbie.config import parse_log_level, parse_scheduled_tasks


@pytest.fixture()
def config_path():
    return join(
        dirname(__file__),
        'resources',
        'test_config.json'
    )


@pytest.fixture()
def invalid_config_path():
    return join(
        dirname(__file__),
        'resources',
        'invalid_config.json'
    )


def test_config_change(config_path):
    init_level = Config.log_level
    init_tasks = Config.scheduled_tasks

    Config.from_json(config_path)

    assert Config.log_level != init_level
    assert Config.log_level == logging.NOTSET

    assert Config.scheduled_tasks != init_tasks
    assert len(Config.scheduled_tasks) == 1
    for task in Config.scheduled_tasks:
        assert isinstance(task, ScheduledTask)


def test_invalid_config_validation(invalid_config_path):
    with pytest.raises(ConfigError):
        Config.from_json(invalid_config_path)


@pytest.mark.parametrize('level, expected',
                         [('NOTSET', logging.NOTSET),
                          ('DEBUG', logging.DEBUG),
                          ('INFO', logging.INFO),
                          ('WARNING', logging.WARNING),
                          ('ERROR', logging.ERROR),
                          ('FATAL', logging.FATAL),
                          ('CRITICAL', logging.CRITICAL)])
def test_parse_log_level(level, expected):
    assert parse_log_level(level) == expected


@pytest.mark.parametrize(
    'tasks, expected',
    [
        (
            [
                {
                    'command': 'foo',
                    'month': '1',
                    'day': '1',
                    'week': '1',
                    'day_of_week': '1',
                    'hour': '1',
                    'minute': '1'
                }
            ],
            [
                ScheduledTask(
                    'foo',
                    None,
                    '1',
                    '1',
                    '1',
                    '1',
                    '1',
                    '1'
                )
            ]
        )
    ]
)
def test_parse_task(tasks, expected):

    tasks = parse_scheduled_tasks(tasks)
    for t, e in zip(tasks, expected):
        assert t == e


def test_empty_schedule():
    tasks = [{'command': 'foo'}]
    tasks = parse_scheduled_tasks(tasks)
    for t in tasks:
        assert t.minute == '*'

