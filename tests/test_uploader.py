import pytest
from sqlalchemy import create_engine
from time import sleep
from os import remove
from rabbie.database import Database, SessionContext
from rabbie import Upload, capture_image
from rabbie import Config, list_files
from rabbie import GoogleService


@pytest.fixture(scope='module')
def uploader():
    yield Upload(GoogleService.DRIVE)

    images = list_files(Config.image_directory, Config.image_extension)
    for img in images:
        remove(img)


@pytest.fixture(scope='function')
def database():

    engine = create_engine('sqlite://')
    context = SessionContext(engine)
    session = context.get_session()

    _database = Database(session)

    yield _database, context

    _database.rollback()

    context.remove_session()


@pytest.mark.api
def test_upload_images(uploader):

    for _ in range(3):
        capture_image()
        sleep(1)

    uploaded = uploader.upload_images()

    assert len(uploaded) >= 3
