import pytest
from sqlalchemy import create_engine
from datetime import datetime, timedelta
from rabbie.database import Database, SessionContext


@pytest.fixture(scope='function')
def database():

    engine = create_engine('sqlite://')
    context = SessionContext(engine)
    session = context.get_session()

    _database = Database(session)

    yield _database

    _database.rollback()

    context.remove_session()


def test_add_and_query_readings(database):

    assert len(database.sensor_readings().all()) == 0

    t0 = datetime(2000, 1, 1, 12, 0, 0)
    for i in range(5):
        database.add_sensor_reading(
            t0 + timedelta(minutes=i),
            'foo',
            1.0,
            'foo_units'
        )
        database.add_sensor_reading(
            t0 + timedelta(minutes=i),
            'bar',
            2.0,
            'bar_units'
        )

    assert database.sensor_readings().count() == 10
    assert database.sensor_readings(label='foo').count() == 5
    assert database.sensor_readings(label='bar').count() == 5
    assert database.sensor_readings(to_timestamp=t0+timedelta(minutes=3)).count() == 6
    assert database.sensor_readings(from_timestamp=t0+timedelta(minutes=3)).count() == 4
    assert database.sensor_readings(synced=True).count() == 0
    assert database.sensor_readings(synced=False).count() == 10


def test_delete(database):

    assert database.sensor_readings().count() == 0

    t0 = datetime(2000, 1, 1, 12, 0, 0)
    for i in range(2):
        database.add_sensor_reading(
            t0 + timedelta(minutes=i),
            'foo',
            1.0,
            'foo_units'
        )

    assert database.sensor_readings().count() == 2

    database.delete_sensor_readings(from_timestamp=t0+timedelta(minutes=1))

    assert database.sensor_readings().count() == 1


def test_modify(database):

    assert database.sensor_readings().count() == 0

    database.add_sensor_reading(
        datetime.utcnow(),
        'foo',
        1.0,
        'foo_units'
    )

    reading = database.sensor_readings().first()

    assert database.sensor_readings(synced=True).count() == 0
    assert database.sensor_readings(synced=False).count() == 1

    reading.last_sync = datetime.utcnow()

    assert database.sensor_readings(synced=True).count() == 1
    assert database.sensor_readings(synced=False).count() == 0
