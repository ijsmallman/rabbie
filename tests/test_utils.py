import os
from os.path import abspath, dirname, join

import pytest

from rabbie import Utils, list_files


def test_hostname():

    Utils.clear_cache()

    assert Utils._hostname is None
    hostname = Utils.hostname
    assert hostname is not None
    assert Utils._hostname is not None


@pytest.fixture
def environ_root():
    directory = "here"
    os.environ['RABBIE_ROOT'] = directory
    yield directory
    del os.environ['RABBIE_ROOT']


def test_root_directory_from_env(environ_root):

    directory = environ_root

    root = Utils.root_directory

    assert root == directory


@pytest.mark.parametrize('extension', ['json', '.json'])
def test_list_files(extension):

    directory = abspath(
        join(
            dirname(__file__),
            'resources'
        )
    )

    paths = list_files(directory, extension)

    assert len(paths) == 2
