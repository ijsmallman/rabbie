import pytest
import numpy as np
from datetime import datetime, timedelta
from rabbie import FirestoreService, google_auth, GoogleService


COLLECTION_NAME = 'dummy'


@pytest.fixture(scope='module')
def credentials():
    return google_auth(GoogleService.FIRESTORE)


@pytest.fixture(scope='module')
def service(credentials):
    service = FirestoreService('rabbie', credentials)
    yield service
    service.delete_collection(COLLECTION_NAME)


@pytest.mark.api
def test_upload(service):
    t0 = datetime(2000, 1, 1, 0, 0, 1)

    for i in range(10):
        doc = {
            'created': datetime.utcnow(),
            'label': 'dummy',
            'value': np.random.rand(),
            'timestamp': t0 + timedelta(minutes=i)
        }

        service.add_data(COLLECTION_NAME, doc)

    docs = service.retrieve_data(COLLECTION_NAME)
    assert len(list(docs)) == 10

    docs = service.retrieve_data(COLLECTION_NAME, from_timestamp=t0+timedelta(minutes=5))
    assert len(list(docs)) == 5

    docs = service.retrieve_data(COLLECTION_NAME, to_timestamp=t0+timedelta(minutes=5))
    assert len(list(docs)) == 5

    docs = service.retrieve_data(
        COLLECTION_NAME,
        from_timestamp=t0+timedelta(minutes=2),
        to_timestamp=t0+timedelta(minutes=5)
    )
    assert len(list(docs)) == 3
