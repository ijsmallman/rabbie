from rabbie.level_sensor import LevelSensor


def test_level():

    level = LevelSensor('test_units', 5.0).measure()

    assert isinstance(level, float)
