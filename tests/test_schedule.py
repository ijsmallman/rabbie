import time
from threading import Thread

import pytest
from sqlalchemy import create_engine

from rabbie import ScheduledTask, Scheduler
from rabbie.database import SessionContext


def signal(counts, *args, **kwargs):
    counts += [1]


@pytest.fixture
def scheduled_tasks():

    counts = [1]
    args = [counts]

    return ([ScheduledTask(
        signal,
        args,
        second=1
    )], counts)


@pytest.fixture(scope='function')
def context():

    engine = create_engine('sqlite://')
    _context = SessionContext(engine)

    yield _context

    _context.remove_session()


def test_scheduler(scheduled_tasks, context):

    counts = scheduled_tasks[1]
    scheduler = Scheduler(scheduled_tasks[0], context)

    thread = Thread(target=scheduler.start, daemon=True)
    thread.start()

    time.sleep(5)

    scheduler.stop()
    thread.join(timeout=5)
    assert not thread.is_alive()

    assert len(counts) > 5
