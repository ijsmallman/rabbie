from os.path import dirname, join

import pytest

from rabbie import DriveService, google_auth, Utils, GoogleService


@pytest.fixture(scope='module')
def credentials():
    return google_auth(GoogleService.DRIVE)


@pytest.fixture(scope='module')
def service(credentials):
    return DriveService(credentials)


@pytest.mark.api
def test_folder_not_found_error(service):

    with pytest.raises(FileNotFoundError):
        service.get_folder_id('bad_folder_name')


@pytest.mark.api
@pytest.mark.parametrize('folder_name, file_name',
                         [(Utils.hostname, 'test_image.jpeg'),
                          (Utils.hostname, 'test_log.log')])
def test_upload(service, folder_name, file_name):

    file_path = join(
        dirname(__file__),
        'resources',
        file_name
    )

    service.upload_file(file_path, folder_name)

    files = service.list_files()
    assert file_name in files.values()
