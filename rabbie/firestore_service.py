import logging
from typing import Generator

from google.oauth2.credentials import Credentials
from google.cloud import firestore
from datetime import datetime

logger = logging.getLogger(__name__)


class FirestoreServiceError(Exception):
    pass


class FirestoreService:

    def __init__(self, project_id: str, credentials: Credentials) -> None:
        self._db = firestore.Client(project_id, credentials)

    def add_data(self, collection: str, data: dict) -> datetime:
        """
        Add data as document to NoSQL database.

        Parameters
        ----------
        collection: str
            Collection name
        data: dict
            Document to add

        Returns
        -------
        sync_time: datetime
            Datetime of sync
        """
        try:
            sync_time = datetime.utcnow()
            data['created'] = sync_time
        except Exception as e:
            logger.error("Failed to add creation date to dict. %s", str(e))
            raise FirestoreServiceError("Failed to set creation date") from e

        try:
            doc_ref = self._db.collection(collection).document()
            doc_ref.set(data)
        except Exception as e:
            logger.error(
                "Failed to add data %s to collection %s. %s",
                str(data),
                collection,
                str(e)
            )
            raise FirestoreServiceError("Failed to add data to collection") from e

        return sync_time

    def retrieve_data(self, collection, from_timestamp: datetime = None, to_timestamp: datetime = None) -> Generator[dict, None, None]:
        """
        Retrieve docs from collection

        Parameters
        ----------
        collection: str
            Collection name
        from_timestamp: datetime
            Optional start datetime
        to_timestamp: datetime
            Optional to datetime

        Returns
        -------
        data_stream: Generator[dict]
        """
        try:
            collection_ref = self._db.collection(collection)
        except Exception as e:
            logger.error("Failed to get collection reference. %s", str(e))
            raise FirestoreServiceError from e

        try:
            if from_timestamp is not None:
                collection_ref = collection_ref.where('timestamp', '>=', from_timestamp)
            if to_timestamp is not None:
                collection_ref = collection_ref.where('timestamp', '<', to_timestamp)
        except Exception as e:
            logger.error("Failed to filter collection data. %s", str(e))
            raise FirestoreServiceError from e

        try:
            docs = collection_ref.stream()
        except Exception as e:
            logger.error("Failed to get data from collection. %s", e)
            raise FirestoreServiceError from e

        return docs

    def delete_collection(self, collection, batch_size: int = 100) -> None:
        """
        Delete all data in collection

        Parameters
        ----------
        collection: str
            Collection name
        batch_size
            Amount of data to delete in one go
        """
        try:
            docs = self._db.collection(collection).limit(batch_size).stream()
        except Exception as e:
            logger.error("Failed to get collection reference. %s", str(e))
            raise FirestoreServiceError from e

        deleted = 0
        for doc in docs:
            logger.debug('Deleting doc with ID %s: %s', str(doc.id), str(doc.to_dict()))
            try:
                doc.reference.delete()
            except Exception as e:
                logger.error("Failed to delete document %s. %s", str(doc.id), str(e))
                raise FirestoreServiceError from e

            deleted = deleted + 1

        if deleted >= batch_size:
            return self.delete_collection(collection, batch_size)
