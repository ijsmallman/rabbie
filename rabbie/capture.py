import logging
from os import makedirs
from os.path import exists, join
from rabbie import init_logger, Config, ConfigError, Camera, CameraError, formatted_timestamp, Utils

logger = logging.getLogger('rabbie.capture')


class CaptureError(Exception):
    pass


def capture_image(*args, **kwargs) -> str:
    """
    Capture image and save to file

    Returns
    -------
    image_path: str

    Raises
    ------
    CaptureError
    """
    exposure_mode = 'auto'
    logger.debug('Capturing image with exposure mode: \"%s\"', exposure_mode)

    if not exists(Config.image_directory):
        makedirs(Config.image_directory)

    stamp = formatted_timestamp()

    annotation = "{}  {}".format(stamp, Utils.hostname)

    image_path = join(
        Config.image_directory,
        "{}.{}".format(
            stamp,
            Config.image_extension)
    )

    try:
        camera = Camera(
            cols=Config.image_width,
            rows=Config.image_height,
            shutter_speed=Config.cam_shutter_speed,
            iso=Config.cam_iso,
            framerate=Config.cam_framerate,
            sensor_mode=Config.cam_sensor_mode
        )

        camera.capture_image(
            file_path=image_path,
            annotation=annotation,
            exposure_mode=exposure_mode
        )

    except CameraError as e:
        raise CaptureError from e

    logger.info("Written image to %s", image_path)

    return image_path


if __name__ == '__main__':

    import sys
    # import argparse
    # parser = argparse.ArgumentParser()
    # parser.add_argument(
    #     '-m',
    #     '--mode',
    #     type=str,
    #     default='auto'
    # )
    # args = parser.parse_args()

    try:
        Config.from_json()
    except ConfigError:
        sys.exit(1)

    init_logger()

    try:
        capture_image()
    except CaptureError:
        sys.exit(1)
