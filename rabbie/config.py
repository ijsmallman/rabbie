import logging
import json
import jsonschema
from typing import List
from os.path import join, exists
from rabbie import ScheduledTask, Utils
from rabbie.utils import classproperty

logger = logging.getLogger(__name__)


def parse_log_level(value: str) -> int:
    """
    Parse the log level

    Parameters
    ----------
    value: str

    Returns
    -------
    level: int
    """
    return logging._nameToLevel[value]


def parse_scheduled_tasks(value: List[dict]) -> List[ScheduledTask]:
    """
    Parse the scheduled tasks

    Parameters
    ----------
    value: List[dict]
        Tasks to be scheduled

    Returns
    -------
    scheduled_tasks: List[ScheduledTask]
    """
    tasks = []
    for task in value:

        command = task['command']
        month = task.get('month', None)
        day = task.get('day', None)
        week = task.get('week', None)
        day_of_week = task.get('day_of_week', None)
        hour = task.get('hour', None)
        minute = task.get('minute', None)
        second = task.get('second', None)
        args = task.get('args', None)

        tasks.append(
            ScheduledTask(
                command,
                args,
                month,
                day,
                week,
                day_of_week,
                hour,
                minute,
                second
            )
        )

    return tasks


class ConfigError(Exception):
    pass


class Config:

    project_id = "rabbie"
    datetime_format = "%Y-%m-%dT%H-%M-%SZ"
    log_level = logging.INFO
    socket_timeout = 300
    image_height = 1080
    image_width = 1920
    cam_shutter_speed = 0
    cam_iso = 0
    cam_framerate = 30
    cam_sensor_mode = 0
    image_directory = 'images'
    image_extension = "jpg"
    log_directory = 'logs'
    log_file_name = 'rabbie'
    log_file_extension = "log"
    db_directory = 'database'
    db_file_name = 'rabbie'
    db_file_extension = "db"
    db_collection = "readings"
    json_file_extension = "json"
    sensor_range = 5
    reading_label = "dummy"
    reading_units = "dummy"
    scheduled_tasks = []

    value_parser_map = {
        'log_level': parse_log_level,
        'scheduled_tasks': parse_scheduled_tasks
    }

    @classproperty
    def log_path(cls) -> str:
        """
        Log file path
        """
        return join(cls.log_directory, "{}.{}".format(cls.log_file_name, cls.log_file_extension))

    @classproperty
    def db_url(cls) -> str:
        """
        Database URL
        """
        return 'sqlite:///{}/{}.{}'.format(cls.db_directory, cls.db_file_name, cls.db_file_extension)

    @classproperty
    def json_path(cls) -> str:
        """
        JSON file path
        """
        return join(cls.db_directory, "{}.{}".format(cls.db_file_name, cls.json_file_extension))

    @classmethod
    def from_json(cls, file_path: str = None):
        """
        Load config from json in configs directory.

        Parameters
        ----------
        file_path: Optional file_path. If None (default), look for config by hostname in configs directory.

        Raises
        ------
        ConfigError
        """
        if not file_path:

            host_name = Utils.hostname

            logger.debug("Loading config for platform %s", host_name)

            file_path = join(
                Utils.root_directory,
                'configs',
                '{}.json'.format(host_name)
            )

        if not exists(file_path):
            logger.warning('Failed to find config %s, using default config', file_path)
        else:
            try:
                with open(join(Utils.package_dir, 'schemas', 'config_schema.json'), 'r') as f:
                    schema = json.load(f)
            except (IOError, json.JSONDecodeError) as e:
                logger.error("Failed to load config schema. %s", e)
                raise ConfigError from e

            try:
                with open(file_path, 'r') as f:
                    config = json.load(f)
            except (IOError, json.JSONDecodeError) as e:
                logger.error("Failed to load config. %s", e)
                raise ConfigError from e

            try:
                jsonschema.validate(
                    config,
                    schema
                )
            except (jsonschema.ValidationError, jsonschema.SchemaError) as e:
                logger.error("Config does not validate against schema. %s", e)
                raise ConfigError from e

            for key, value in config.items():
                if key in Config.value_parser_map.keys():
                    value = Config.value_parser_map[key](value)

                setattr(cls, key, value)
