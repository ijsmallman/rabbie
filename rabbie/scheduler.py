import logging
from typing import List
from apscheduler.schedulers.blocking import BlockingScheduler
from rabbie import init_logger, Config, ConfigError, ScheduledTask, set_socket_timeout
from rabbie.database import SessionContext


logger = logging.getLogger('rabbie.scheduler')


class Scheduler:

    def __init__(self, tasks: List[ScheduledTask], context: SessionContext = None) -> None:
        """
        Run scheduled tasks with Python process scheduler

        Parameters
        ----------
        tasks: List[ScheduledTask]
            Tasks to run to schedule
        """

        self._scheduler = BlockingScheduler()

        if context is not None:
            self._context = context
        else:
            self._context = SessionContext()

        if not tasks:
            logger.warning("No jobs to schedule")

        for task in tasks:
            self._scheduler.add_job(
                task.command,
                'cron',
                month=task.month,
                day=task.day,
                week=task.week,
                day_of_week=task.day_of_week,
                hour=task.hour,
                minute=task.minute,
                args=task.args,
                kwargs={'context': self._context}
            )
            logger.info("Scheduled task: %s", task)

    def start(self) -> None:
        """
        Start the scheduler
        """
        logger.info("Scheduler start")
        self._scheduler.start()

    def stop(self) -> None:
        """
        Stop the scheduler
        """
        logger.info("Scheduler stop")
        self._scheduler.shutdown()


if __name__ == '__main__':
    import sys

    try:
        Config.from_json()
    except ConfigError:
        sys.exit(1)

    init_logger()

    set_socket_timeout(Config.socket_timeout)

    s = Scheduler(Config.scheduled_tasks)

    try:
        s.start()
    except KeyboardInterrupt:
        pass
