import os
import pickle
import logging
from enum import Enum
from os.path import join
from typing import List
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.auth.exceptions import RefreshError
from google.oauth2.credentials import Credentials
from rabbie import Utils

SECRETS_PATH = join(
    Utils.root_directory,
    'secrets'
)

logger = logging.getLogger(__name__)


class CredentialPaths:
    """Paths to credential files"""

    def __init__(
            self,
            credentials_filename: str,
            token_filename: str,
            scopes: List[str]
    ):
        self._credentials_filename = credentials_filename
        self._token_filename = token_filename
        self._scopes = scopes

    @property
    def credentials(self):
        """Oauth2 credentials JSON file"""
        return join(SECRETS_PATH, self._credentials_filename)

    @property
    def token(self):
        """Pickled credentials object"""
        return join(SECRETS_PATH, self._token_filename)

    @property
    def scopes(self):
        """Authorisation scopes"""
        return self._scopes


class GoogleService(Enum):
    """Google services supported"""
    DRIVE = 0
    FIRESTORE = 1


class GoogleAuthError(Exception):
    pass


CREDENTIAL_PATHS = {
    GoogleService.DRIVE: CredentialPaths(
        "credentials.json",
        "token.pickle",
        ['https://www.googleapis.com/auth/drive']
    ),
    GoogleService.FIRESTORE: CredentialPaths(
        "credentials.json",
        "gcloud_token.pickle",
        ['https://www.googleapis.com/auth/cloud-platform']  # may also need https://www.googleapis.com/auth/datastore
    )
}


def google_auth(service: GoogleService, headless: bool = False) -> Credentials:
    """
    Generate Google Credentials object

    Parameters
    ----------
    service: GoogleService
        Google service to authorise
    headless: bool

    Returns
    -------
    credentials: Credentials
    """
    try:
        paths = CREDENTIAL_PATHS[service]
    except KeyError:
        logger.error("Unrecognised service: %s", service)
        raise GoogleAuthError

    credentials = None
    if os.path.exists(paths.token):
        try:
            with open(paths.token, 'rb') as token:
                credentials = pickle.load(token)
        except IOError as e:
            logger.error("Failed to load api token. %s", str(e))
            raise GoogleAuthError

    # If there are no (valid) credentials available, let the user log in.
    if not credentials or not credentials.valid:
        if headless:
            logger.error("Failed to authorise application")
            raise GoogleAuthError

        else:
            if credentials and credentials.expired and credentials.refresh_token:
                logger.debug("Credentials expired, refreshing token")
                try:
                    credentials.refresh(Request())
                except (RefreshError, KeyError) as e:
                    logger.error("Failed to refresh credentials. %s", str(e))
                    raise GoogleAuthError from e
            else:
                logger.warning("No valid credentials found, requesting new credentials")
                try:
                    flow = InstalledAppFlow.from_client_secrets_file(
                        paths.credentials,
                        paths.scopes
                    )
                except (IOError, ValueError) as e:
                    logger.error("Failed to load credentials path %s. %s", paths.credentials, str(e))
                    raise GoogleAuthError from e

                flow.authorization_url(
                    access_type='offline',
                    include_granted_scopes='true'
                )
                credentials = flow.run_local_server()

            # Save the credentials for the next run
            try:
                with open(paths.token, 'wb') as token:
                    pickle.dump(credentials, token)
            except IOError as e:
                logger.error("Failed to save api token to %s. %s", paths.token, str(e))
                raise GoogleAuthError from e

    if not credentials.refresh_token:
        logger.warning("No refresh token supplied")

    return credentials
