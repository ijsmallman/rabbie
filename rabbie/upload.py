import logging
from os import remove
from os.path import exists, basename
from typing import List
from rabbie import init_logger, Config, ConfigError, google_auth, GoogleAuthError, DriveService, \
    DriveServiceError, Utils, formatted_timestamp, set_socket_timeout, list_files, GoogleService, \
    FirestoreService, FirestoreServiceError
from rabbie.database import SessionContext, Database

logger = logging.getLogger('rabbie.upload')


class UploadError(Exception):
    pass


class Upload:

    def __init__(self, service: GoogleService, context: SessionContext = None) -> None:
        """
        File upload wrapper for Google authentication and drive service

        Parameters
        ----------
        service: GoogleService
        context: SessionContext
        """
        try:
            self._credentials = google_auth(service)
        except GoogleAuthError as e:
            raise UploadError from e

        if service == GoogleService.DRIVE:
            self._service = DriveService(self._credentials)
        elif service == GoogleService.FIRESTORE:
            self._service = FirestoreService(Config.project_id, self._credentials)
        else:
            logger.error("Unrecognised Google service: %s", service)
            raise UploadError

        self._context = context

    def upload_readings(self) -> List[str]:
        """
        Upload readings to Google Firestore

        Returns
        -------
        uploaded_readings: List[str]
        """
        if self._context is None:
            self._context = SessionContext()

        uploaded_entries = []
        try:
            with Database(self._context.get_session()) as db:
                for entry in db.sensor_readings(synced=False):

                    data = entry.to_dict()
                    uploaded_entries.append(data)

                    try:
                        sync_time = self._service.add_data(Config.db_collection, data)
                        entry.last_sync = sync_time
                        logger.debug("Uploaded reading: %s", str(data))

                    except FirestoreServiceError:
                        pass

        except Exception as e:
            logger.error("Failed to upload readings to cloud. %s", str(e))

        logger.debug("Uploaded %i readings", len(uploaded_entries))

        return uploaded_entries

    def upload_log(self) -> List[str]:
        """
        Upload current log to Google Drive under a folder name with computer hostname

        Returns
        -------
        uploaded_logs: List[str]
            Logs to successfully uploaded plots
        """
        if not exists(Config.log_path):
            logger.debug("Log upload skipped: No log found at %s", Config.log_path)
            return []

        file_name = '{}.log'.format(formatted_timestamp())

        uploaded_paths = []
        try:
            self._service.upload_file(Config.log_path, Utils.hostname, file_name)
            uploaded_paths.append(Config.log_path)
        except DriveServiceError as e:
            logger.error("Failed to upload log at path: %s. %s", Config.log_path, e)

        return uploaded_paths

    def upload_images(self) -> List[str]:
        """
        Upload images in image folder to Google Drive under folder named with computer hostname

        Returns
        -------
        uploaded_images: List[str]
            Paths to successfully uploaded images
        """
        image_paths = list_files(Config.image_directory, Config.image_extension)
        if not image_paths:
            logger.debug("Image upload skipped: No images found in %s", Config.image_directory)
            return []
        else:
            logger.debug("Found %i image(s) in %s", len(image_paths), Config.image_directory)

        uploaded_paths = []
        for path in image_paths:
            file_name = basename(path)
            try:
                self._service.upload_file(path, Utils.hostname, file_name)
                uploaded_paths.append(path)
            except DriveServiceError as e:
                logger.error("Failed to upload image at path: %s. %s", path, e)

        logger.debug("Uploaded %i images", len(uploaded_paths))
        
        # Delete uploaded images to stop repeated uploads
        for path in uploaded_paths:
            try:
                remove(path)
                logger.debug("Deleted %s", path)
            except Exception as e:
                logger.error("Failed to delete image at %s. %s", path, str(e))

        return uploaded_paths


def upload_log(*args, uploader: Upload = None, **kwargs) -> List[str]:
    """
    Upload log to cloud
    
    Returns
    -------
    uploaded_logs: List[str]
        Paths to uploaded logs
    """
    uploaded = []
    
    if uploader is None:
        try:
            uploader = Upload(GoogleService.DRIVE)
        except Exception as e:
            logger.error("Failed to create uploader. %s", str(e))
            return uploaded

    try:
        uploaded += uploader.upload_log()
    except Exception as e:
        logger.error("Error uploading log: %s", str(e))
    return uploaded


def upload_images(*args, uploader: Upload = None, **kwargs) -> List[str]:
    """
    Upload images to cloud

    Returns
    -------
    uploaded_images: List[str]
        Paths to uploaded images
    """
    uploaded = []

    if uploader is None:
        try:
            uploader = Upload(GoogleService.DRIVE)
        except Exception as e:
            logger.error("Failed to create uploader. %s", str(e))
            return uploaded

    try:
        uploaded += uploader.upload_images()
    except Exception as e:
        logger.error("Error uploading image: %s", str(e))

    return uploaded


def upload_readings(*args, uploader: Upload = None, **kwargs) -> List[dict]:
    """
    Upload readings to cloud

    Returns
    -------
    uploaded_readings: List[dict]
        Uploaded readings
    """
    uploaded = []
    
    if uploader is None:
        try:
            uploader = Upload(GoogleService.FIRESTORE)
        except Exception as e:
            logger.error("Failed to create uploader. %s", str(e))
            return uploaded

    try:
        uploaded += uploader.upload_readings()
    except Exception as e:
        logger.error("Error uploading readings: %s", str(e))

    return uploaded


def upload_all(
    *args,
    context: SessionContext = None,
    log: bool = True,
    images: bool = True,
    readings: bool = True,
    **kwargs
) -> None:
    """
    Sync data to cloud

    Parameters
    ----------
    context: SessionContext
    log: bool
        Upload log to cloud?
    images: bool
        Upload images to cloud?
    readings: bool
        Upload json to cloud?
    """
    sources = []
    if log:
        sources.append('logs')
    if images:
        sources.append('images')
    if readings:
        sources.append('readings')
    logger.info("Synchronising %s to cloud", sources)

    try:
        if log or images:
            drive_uploader = Upload(GoogleService.DRIVE, context)
        if readings:
            firestore_uploader = Upload(GoogleService.FIRESTORE, context)
    except (UploadError, Exception):
        logger.error("Error initialising uploader")
        return

    if images:
        upload_images(drive_uploader)

    if readings:
        upload_readings(firestore_uploader)

    if log:
        upload_log(drive_uploader)


if __name__ == '__main__':
    import sys
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--log', action='store_true', help="Upload log specifically")
    parser.add_argument('--image', action='store_true', help="Upload image specifically")
    parser.add_argument('--readings', action='store_true', help="Upload readings specifically")
    args = parser.parse_args()

    try:
        Config.from_json()
    except ConfigError:
        sys.exit(1)

    init_logger()

    set_socket_timeout(Config.socket_timeout)

    upload_all(log=args.log, images=args.image, readings=args.readings)
