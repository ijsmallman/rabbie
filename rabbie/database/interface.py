from sqlalchemy.orm.session import Session
from sqlalchemy.orm.query import Query
from sqlalchemy.exc import IntegrityError
from datetime import datetime
import logging
from rabbie.database.models import SensorReading


logger = logging.getLogger('rabbie.database.interface')


class Database:

    def __init__(self, session: Session):
        """
        Main database access class

        Parameters
        ----------
        session: Session
        """
        self._session = session

    def add_sensor_reading(
            self,
            timestamp: datetime,
            label: str,
            value: float,
            units: str
    ) -> None:
        """
        Add a sensor reading to the database

        Parameters
        ----------
        timestamp: datetime
            Date-time of reading
        label: str
            Name of sensor reading
        value: float
            Sensor value
        units: str
            Units of float
        """
        reading = SensorReading(
            label=label,
            value=value,
            units=units,
            timestamp=timestamp
        )

        self._session.add(reading)

        logger.debug("Added sensor reading %s", reading)

    def sensor_readings(
            self,
            label: str = None,
            synced: bool = None,
            from_timestamp: datetime = None,
            to_timestamp: datetime = None
    ) -> Query:
        """
        Query database for sensor readings

        Parameters
        ----------
        label: str
            Reading label
        from_timestamp
            Optional start date-time
        to_timestamp: datetime
            Optional end date-time
        synced: bool
            Optionally only return synced (true) or unsynced (false) events

        Returns
        -------
        readings: Query
        """
        filter_args = []
        log_args = []
        if label is not None:
            filter_args.append(
                SensorReading.label == label
            )
            log_args.append(
                "label: '{}'".format(label)
            )
        if from_timestamp is not None:
            filter_args.append(
                SensorReading.timestamp >= from_timestamp
            )
            log_args.append(
                "from_timestamp: '{}'".format(from_timestamp)
            )
        if to_timestamp is not None:
            filter_args.append(
                SensorReading.timestamp < to_timestamp
            )
            log_args.append(
                "to_timestamp: '{}'".format(to_timestamp)
            )
        if synced is not None:
            if synced:
                filter_args.append(
                    SensorReading.last_sync.isnot(None)
                )
            else:
                filter_args.append(
                    SensorReading.last_sync.is_(None)
                )
            log_args.append(
                "synced: '{}'".format(synced)
            )

        readings = self._session.query(SensorReading).filter(*filter_args)

        logger.debug("Queried sensor readings with filter args: %s", log_args)

        return readings

    def delete_sensor_readings(
            self,
            label: str = None,
            synced: bool = None,
            from_timestamp: datetime = None,
            to_timestamp: datetime = None
    ) -> None:
        """
        Delete sensor readings from database

        Parameters
        ----------
        label: str
            Reading label
        from_timestamp
            Optional start date-time
        to_timestamp: datetime
            Optional end date-time
        synced: bool
            Optionally only return synced (true) or unsynced (false) events
        """
        readings = self.sensor_readings(label, synced, from_timestamp, to_timestamp)

        for reading in readings:
            self._session.delete(reading)
            logger.debug("Deleted %s", reading)

    def commit(self) -> None:
        try:
            self._session.commit()
        except IntegrityError as e:
            logger.error("Integrity error, rolling back: %s", e)
            self.rollback()

    def flush(self) -> None:
        self._session.flush()

    def rollback(self) -> None:
        self._session.rollback()

    def __enter__(self) -> 'Database':
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        self.commit()
