from sqlalchemy import Column, Integer, String, Float, DateTime
from rabbie.database.models import Base


class SensorReading(Base):
    __tablename__ = 'sensor_readings'

    id = Column(Integer, primary_key=True)
    label = Column(String, nullable=False)
    value = Column(Float, nullable=False)
    units = Column(String, nullable=False)
    timestamp = Column(DateTime, nullable=False)
    last_sync = Column(DateTime, nullable=True)

    def __repr__(self):
        return "<SensorReading(label='{}', value='{}', units='{}', timestamp='{}', last_sync='{}')>".format(
            self.label,
            self.value,
            self.units,
            self.timestamp,
            self.last_sync
        )

    def to_dict(self):
        return {
            'timestamp': self.timestamp,
            'label': self.label,
            'value': self.value,
            'units': self.units,
        }
