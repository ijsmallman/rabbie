from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.orm.session import Session
from os.path import exists
from os import makedirs
import logging
from rabbie.database.models import Base
from rabbie import Config


logger = logging.getLogger(__name__)


class SessionContext:

    def __init__(self, engine: Engine = None):

        if not exists(Config.db_directory):
            makedirs(Config.db_directory)

        if engine is None:
            self._engine = create_engine(Config.db_url)
        else:
            self._engine = engine

        Base.metadata.create_all(bind=self._engine)

        session_factory = sessionmaker(bind=self._engine)

        self._Session = scoped_session(session_factory)

    def get_session(self) -> Session:
        """
        Get an database session instance

        Returns
        -------
        session: Session
        """
        return self._Session()

    def remove_session(self) -> None:
        """
        Remove thread local instance of Session
        """
        self._Session.remove()
