import logging
from datetime import datetime
from os import makedirs
from os.path import exists
from rabbie import init_logger, Config, ConfigError, LevelSensor, LevelSensorError
from rabbie.database import Database, SessionContext

logger = logging.getLogger('rabbie.measure_level')


class MeasurementError(Exception):
    pass


def store_measurement(level: float, timestamp: datetime = None, context: SessionContext = None) -> None:
    """
    Store measurement in local database

    Parameters
    ----------
    level: float
    timestamp: datetime
    context: SessionContext
    """
    if not exists(Config.db_directory):
        makedirs(Config.db_directory)

    if timestamp is None:
        timestamp = datetime.utcnow()

    if context is None:
        context = SessionContext()

    with Database(context.get_session()) as db:
        db.add_sensor_reading(
            timestamp,
            Config.reading_label,
            level,
            Config.reading_units
        )


def measure_level(*args, context: SessionContext = None, **kwargs) -> None:
    """
    Measure level and store in database

    Raises
    ------
    CaptureError
    """
    try:
        level = LevelSensor(Config.reading_units, Config.sensor_range).measure()
        logger.info("Water level: %s %s", level, Config.reading_units)

    except LevelSensorError as e:
        logger.error("Failed to measure level: %s", e)
        raise MeasurementError from e

    timestamp = datetime.utcnow()

    try:
        store_measurement(level, timestamp, context=context)
    except Exception as e:
        logger.error("Failed to store measurement: %s", e)


if __name__ == '__main__':

    import sys

    try:
        Config.from_json()
    except ConfigError:
        sys.exit(1)

    init_logger()

    try:
        measure_level()
    except MeasurementError:
        sys.exit(1)
