import subprocess
__version__ = subprocess.check_output(["git", "describe", "--always"]).strip().decode('ascii')
