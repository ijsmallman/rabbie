import logging
from typing import Optional, Iterable

logger = logging.getLogger(__name__)


class ScheduledTask:

    def __init__(
            self,
            command: str,
            args: Optional[Iterable] = None,
            month: Optional[str] = None,
            day: Optional[str] = None,
            week: Optional[str] = None,
            day_of_week: Optional[str] = None,
            hour: Optional[str] = None,
            minute: Optional[str] = None,
            second: Optional[str] = None
    ):
        self.command = command
        self.args = args
        self.month = month
        self.day = day
        self.week = week
        self.day_of_week = day_of_week
        self.hour = hour
        self.minute = minute
        self.second = second

        if all(x is None for x in [month, day, week, day_of_week, hour, minute, second]):
            logger.warning("Schedule not specified, defaulting to once every minute")
            self.minute = '*'

    def __repr__(self):
        return "ScheduledTask(command=\'{}\', month=\'{}\', day=\'{}\', week=\'{}\', day_of_week=\'{}\', "\
               "hour=\'{}\', minute=\'{}\', second=\'{}\')".format(
                    self.command, self.month, self.day, self.week, self.day_of_week, self.hour, self.minute, self.second
                )

    def __eq__(self, other):
        if not isinstance(other, ScheduledTask):
            raise NotImplementedError

        return all(x == y for x, y in zip(
                [self.command, self.args, self.month, self.day, self.week, self.day_of_week, self.hour, self.minute, self.second],
                [other.command, other.args, other.month, other.day, other.week, other.day_of_week, other.hour, other.minute, other.second]
            )
        )

