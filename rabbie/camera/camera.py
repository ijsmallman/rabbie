import logging


logger = logging.getLogger(__name__)


class CameraError(Exception):
    pass


class BaseCamera:
    """
    Abstract Camera
    """

    def __init__(
            self,
            cols: int,
            rows: int,
            *args,
            shutter_speed: int = 0,
            iso: int = 0,
            framerate: float = 30,
            **kwargs
    ) -> None:
        """
        Initialise base camera class

        Parameters
        ----------
        cols: int
            Image column count
        rows: int
            Image rows count
        shutter_speed: int
            Camera shutter speed. 0 for auto
        iso: int
            Camera ISO setting. 0 for auto
        """
        self.rows = rows
        self.cols = cols
        self.shutter_speed = shutter_speed
        self.iso = iso
        self.framerate = framerate
        logger.debug("Resolution: (%s, %s)", self.cols, self.rows)
        logger.debug("Framerate: %s", self.framerate)
        logger.debug("Shutter speed: %s", self.shutter_speed)
        logger.debug("ISO: %s", self.iso)
        logger.debug("Initialised BaseCamera")

    def capture_image(
            self,
            file_path: str,
            annotation: str,
            *args,
            **kwargs
    ) -> None:
        """
        Capture new image

        Parameters
        ----------
        file_path: str
            Destination to save image
        annotation: str
            Text to watermark image with

        Raises
        ------
        CameraError
        """
        raise NotImplementedError
