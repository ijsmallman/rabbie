import picamera
import logging
import time
from rabbie.camera import BaseCamera, CameraError

logger = logging.getLogger(__name__)


class RpiCamera(BaseCamera):

    def __init__(self, cols: int, rows: int, *args, **kwargs) -> None:
        super().__init__(cols, rows, *args, **kwargs)
        self.sensor_mode = kwargs.get('sensor_mode', 0)
        logger.debug("Sensor mode: %s", self.sensor_mode)
        logger.debug("Initialised RpiCamera")

    def capture_image(
            self,
            file_path: str,
            annotation: str = '',
            exposure_mode: str = 'auto',
            *args,
            **kwargs
    ) -> None:
        """
        Capture new image

        Parameters
        ----------
        file_path: str
            Destination to save image.
        annotation: str
            Text to watermark image with
        exposure_mode: str
            Camera exposure mode:
            {'off', 'auto', 'night', 'nightpreview',
            'backlight', 'spotlight', 'sports', 'snow', 'beach',
            'verylong', 'fixedfps', 'antishake', 'fireworks'}

        Raises
        ------
        CameraError
        """
        try:
            with picamera.PiCamera(
                resolution=(self.cols, self.rows),
                framerate=self.framerate,
                sensor_mode=self.sensor_mode
            ) as cam:

                if self.shutter_speed > 0:
                    cam.exposure_mode = 'off'
                    cam.shutter_speed = self.shutter_speed
                else:
                    cam.exposure_mode = exposure_mode

                if self.iso > 0:
                    cam.iso = self.iso

                if annotation:
                    cam.annotate_text = annotation
                    cam.annotate_background = picamera.Color('slategrey')

                time.sleep(2)

                cam.capture(file_path)

        except Exception as e:
            logger.error('Failed to capture image. %s', e)
            raise CameraError from e
