from .camera import BaseCamera, CameraError
try:
    from .rpi_camera import RpiCamera as Camera
except ImportError:
    from .mock_camera import MockCamera as Camera
