import logging
from rabbie.camera import BaseCamera
import cv2
import numpy as np

logger = logging.getLogger(__name__)


class MockCamera(BaseCamera):

    def __init__(self, cols: int, rows: int, *args, **kwargs) -> None:
        super().__init__(cols, rows, *args, **kwargs)

        logger.warning('Initializing mock camera')

    def capture_image(
            self,
            file_path: str,
            annotation: str,
            *args,
            **kwargs
    ) -> None:
        """
        Save mock image to file

        Parameters
        ----------
        file_path: str
            Destination to save image.
        annotation: str
            Text to watermark image with

        Raises
        ------
        CameraError
        """
        frame = np.zeros((self.rows, self.cols, 3), dtype=np.uint8)

        cv2.putText(
            frame,
            annotation,
            (10, self.rows - 10),
            cv2.FONT_HERSHEY_SIMPLEX,
            0.5,
            (255, 255, 255)
        )

        cv2.imwrite(file_path, frame)
