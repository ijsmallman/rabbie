import logging
import serial
import time
from rabbie.level_sensor import BaseLevelSensor, LevelSensorError

logger = logging.getLogger(__name__)


COM_PORT = '/dev/serial0'
BAUD_RATE = 9600
READ_ATTEMPTS = 10
MESSAGE_LENGTH = 6
START_FLAG = 'R'
END_FLAG = '\r'
SLEEP_TIME = 0.2


class RpiLevelSensor(BaseLevelSensor):

    def __init__(self, units: str, sensor_range: float) -> None:
        super().__init__(units, sensor_range)

        logger.debug('Initializing level sensor')

    def measure(self) -> float:
        """
        Measure level

        Returns
        -------
        level: float
        """
        try:
            with serial.Serial(COM_PORT, BAUD_RATE) as ser:

                ser.flushInput()

                for _ in range(READ_ATTEMPTS):

                    if ser.in_waiting >= MESSAGE_LENGTH:

                        buffer = ser.read(ser.in_waiting).decode('ascii')

                        for i, b in enumerate(buffer):

                            if b == START_FLAG:

                                data = buffer[i+1: i+MESSAGE_LENGTH-1]
                                val = float(data)

                                if self._units == 'm':
                                    val = val / 1000

                                end_flag = buffer[i+MESSAGE_LENGTH-1]

                                if end_flag != END_FLAG:
                                    raise LevelSensorError(
                                        "Invalid end flag (%s) for buffer: %s",
                                        end_flag,
                                        buffer
                                    )

                                return self._range - val

                    time.sleep(SLEEP_TIME)

        except (serial.serialutil.SerialException, ValueError) as e:
            logger.error("Failed to read value from serial port: %s", e)
            raise LevelSensorError from e

        raise LevelSensorError("Timeout waiting for sensor reading")


if __name__ == '__main__':

    sensor = RpiLevelSensor('m', 5.0)
    level = sensor.measure()
    print(level)
