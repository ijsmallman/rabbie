import logging

logger = logging.getLogger(__name__)


class LevelSensorError(Exception):
    pass


class BaseLevelSensor:
    """
    Abstract level sensor
    """
    def __init__(self, units: str, sensor_range: float):
        self._units = units
        self._range = sensor_range

    def measure(self) -> float:
        """
        Measure level

        Returns
        -------
        level: float
        """
        raise NotImplementedError
