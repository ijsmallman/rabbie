import logging
import numpy as np
from rabbie.level_sensor import BaseLevelSensor

logger = logging.getLogger(__name__)


class MockLevelSensor(BaseLevelSensor):

    def __init__(self, units: str, sensor_range: float) -> None:
        super().__init__(units, sensor_range)

        logger.warning('Initializing mock level sensor')

    def measure(self) -> float:
        """
        Measure level

        Returns
        -------
        level: float
        """
        level = self._range - np.random.uniform()
        return level
