from .sensor import BaseLevelSensor, LevelSensorError
try:
    from .rpi_sensor import RpiLevelSensor as LevelSensor
except ImportError:
    from .mock_sensor import MockLevelSensor as LevelSensor
