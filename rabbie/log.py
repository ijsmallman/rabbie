import logging
import time
from logging import StreamHandler
from logging.handlers import TimedRotatingFileHandler
from os import makedirs
from os.path import exists
from rabbie import __version__
from rabbie import Config, Utils


def init_logger():

    logger = logging.getLogger('rabbie')

    logger.setLevel(Config.log_level)

    if not exists(Config.log_directory):
        makedirs(Config.log_directory)

    formatter = logging.Formatter(
        fmt='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S %Z'
    )
    formatter.converter = time.gmtime

    handler = TimedRotatingFileHandler(
        Config.log_path,
        when="d",
        interval=30,
        backupCount=0
    )
    handler.setFormatter(formatter)
    handler.setLevel(Config.log_level)

    logger.addHandler(handler)

    handler = StreamHandler()
    handler.setFormatter(formatter)
    handler.setLevel(Config.log_level)

    logger.addHandler(handler)

    logger.info("Rabbie version: %s", __version__)

    Utils.clear_cache()
