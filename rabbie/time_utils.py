from datetime import datetime
from rabbie import Config


def formatted_timestamp() -> str:
    """
    Get time as a formatted string
    """
    return datetime.utcnow().strftime(Config.datetime_format)
