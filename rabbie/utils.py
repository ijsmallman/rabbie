import logging
from typing import List
from os import getenv, listdir
from os.path import abspath, dirname, join, exists


logger = logging.getLogger('rabbie.utils')


class classproperty(property):
    def __get__(self, cls, owner):
        return classmethod(self.fget).__get__(None, owner)()


class Utils:

    _hostname = None

    @classproperty
    def root_directory(cls) -> str:
        return getenv(
            'RABBIE_ROOT',
            abspath(dirname(dirname(__file__)))
        )

    @classproperty
    def package_dir(cls) -> str:
        return abspath(dirname(__file__))

    @classproperty
    def hostname(cls) -> str:

        if cls._hostname is None:
            hostname_path = join(cls.root_directory, 'hostname.txt')
            if exists(hostname_path):
                try:
                    with open(hostname_path, 'r') as f:
                        cls._hostname = f.read().split('\n')[0]
                        logger.debug('Read hostname \"%s\" from %s', cls._hostname, hostname_path)
                except IOError as e:
                    logger.error("Failed to read hostname from %s", hostname_path)
                    raise IOError from e
            else:
                import platform
                cls._hostname = platform.node()
                logger.debug("Read hostname \"%s\" from platform.node()", cls._hostname)

        return cls._hostname

    @classmethod
    def clear_cache(cls) -> None:

        cls._hostname = None


def set_socket_timeout(timeout: int) -> None:
    """
    Set default socket timeout

    Parameters
    ----------
    timeout: int
        Timeout in seconds
    """
    import socket
    socket.setdefaulttimeout(timeout)
    logger.debug("Set socket timeout to %is", timeout)


def list_files(directory: str, extension: str) -> List[str]:
    """
    List all files in directory with extension

    Parameters
    ----------
    directory: str
        Path to directory
    extension: str
        File extension

    Returns
    -------
    file_paths: List[str]
    """
    return [join(directory, f) for f in listdir(directory)
            if f.endswith(extension)]
